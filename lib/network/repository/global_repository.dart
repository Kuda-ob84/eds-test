import 'dart:io';

import 'package:eclipse_test/network/models/dto_models/request/comment.dart';
import 'package:eclipse_test/network/models/dto_models/response/albums.dart';
import 'package:eclipse_test/network/models/dto_models/response/comments.dart';
import 'package:eclipse_test/network/models/dto_models/response/photos.dart';
import 'package:eclipse_test/network/models/dto_models/response/posts.dart';
import 'package:eclipse_test/network/models/dto_models/response/users.dart';
import 'package:eclipse_test/network/repository/hive_repository.dart';
import 'package:eclipse_test/network/services/network_service.dart';

class GlobalRepository {
  late final NetworkService _networkService;
  late final HiveRepository _hiveRepository;

  void init(NetworkService networkService, HiveRepository hiveRepository) {
    _networkService = networkService;
  }

  Future<List<UsersResponse>> getProfiles() async {
    return _networkService.getProfiles();
  }

  Future<List<PostsResponse>> getPosts() async {
    return _networkService.getPosts();
  }

  Future<List<PostsResponse>> getUserPosts(int userId) async {
    return _networkService.getUserPosts(userId);
  }

  Future<List<CommentsResponse>> getComments() async {
    return _networkService.getComments();
  }

  Future<List<AlbumsResponse>> getAlbums(int userId) async {
    return _networkService.getAlbums(userId);
  }

  Future<List<PhotosResponse>> getPhotos(int albumId) async {
    return _networkService.getPhotos(albumId);
  }

  Future<bool> addComment(int postId, CommentRequest commentRequest) async {
    return _networkService.addComment(postId, commentRequest);
  }
}
