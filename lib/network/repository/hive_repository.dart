import 'package:hive/hive.dart';

class HiveRepository {
  late final Box<String> _stringsBox;

  Future<void> init() async {
    registerAdapters();
    _stringsBox = await Hive.openBox<String>(BoxNames.stringBox);
  }

  void registerAdapters() {}

  Future<void> setPostsResponse(String code) async {
    _stringsBox.put(BoxKeys.posts, code);
  }

  String? getPostsResponse() {
    return _stringsBox.get(BoxKeys.posts, defaultValue: '');
  }

  Future<void> deletePostsResponse() async {
    await _stringsBox.delete(BoxKeys.posts);
  }

  Future<void> setUsersResponse(String code) async {
    _stringsBox.put(BoxKeys.users, code);
  }

  String? getUsersResponse() {
    return _stringsBox.get(BoxKeys.users, defaultValue: '');
  }

  Future<void> deleteUsersResponse() async {
    await _stringsBox.delete(BoxKeys.users);
  }

  Future<void> setCommentsResponse(String code) async {
    _stringsBox.put(BoxKeys.comments, code);
  }

  String? getCommentsResponse() {
    return _stringsBox.get(BoxKeys.comments, defaultValue: '');
  }

  Future<void> deleteCommentsResponse() async {
    await _stringsBox.delete(BoxKeys.comments);
  }
}

class BoxNames {
  static const String stringBox = 'string_box';
}

class BoxKeys {
  static const String posts = 'posts';
  static const String users = 'users';
  static const String comments = 'comments';
}

class HiveTypeIds {}
