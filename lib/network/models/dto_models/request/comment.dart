class CommentRequest {
  final String name;
  final String email;
  final String description;

  CommentRequest({
    required this.name,
    required this.email,
    required this.description,
  });

  Map<String, dynamic> toJson() {
    return {
      "name": name,
      "email": email,
      "body": description,
    };
  }
}
