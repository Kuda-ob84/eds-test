import 'dart:convert';

List<PhotosResponse> photosResponseFromJson(String str) => List<PhotosResponse>.from(json.decode(str).map((x) => PhotosResponse.fromJson(x)));

class PhotosResponse {
  PhotosResponse({
     this.albumId,
     this.id,
     this.title,
     this.url,
     this.thumbnailUrl,
  });

  final int? albumId;
  final int? id;
  final String? title;
  final String? url;
  final String? thumbnailUrl;

  factory PhotosResponse.fromJson(Map<String, dynamic> json) => PhotosResponse(
    albumId: json["albumId"],
    id: json["id"],
    title: json["title"],
    url: json["url"],
    thumbnailUrl: json["thumbnailUrl"],
  );
}
