import 'dart:convert';

List<CommentsResponse> commentsResponseFromJson(String str) => List<CommentsResponse>.from(json.decode(str).map((x) => CommentsResponse.fromJson(x)));

class CommentsResponse {
  CommentsResponse({
    this.postId,
    this.id,
    this.name,
    this.email,
    this.body,
  });

  final int? postId;
  final int? id;
  final String? name;
  final String? email;
  final String? body;

  factory CommentsResponse.fromJson(Map<String, dynamic> json) => CommentsResponse(
    postId: json["postId"],
    id: json["id"],
    name: json["name"],
    email: json["email"],
    body: json["body"],
  );
}
