import 'dart:convert';

import 'package:eclipse_test/network/models/dto_models/response/users.dart';

List<PostsResponse> postsResponseFromJson(String str) => List<PostsResponse>.from(json.decode(str).map((x) => PostsResponse.fromJson(x)));

class PostsResponse {
  PostsResponse({
    required this.userId,
    required this.id,
    required this.title,
    required this.body,
    this.user,
    this.date,
  });

  final int? userId;
  final int? id;
  final String? title;
  final String? body;
  UsersResponse? user;
  DateTime? date;
  String? imageUrl;

  factory PostsResponse.fromJson(Map<String, dynamic> json) => PostsResponse(
    userId: json["userId"],
    id: json["id"],
    title: json["title"],
    body: json["body"],
  );
}
