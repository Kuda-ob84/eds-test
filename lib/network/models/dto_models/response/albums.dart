import 'dart:convert';

import 'package:eclipse_test/network/models/dto_models/response/photos.dart';

List<AlbumsResponse> albumsResponseFromJson(String str) => List<AlbumsResponse>.from(json.decode(str).map((x) => AlbumsResponse.fromJson(x)));

class AlbumsResponse {
  AlbumsResponse({
     this.userId,
     this.id,
     this.title,
  }) : photos = [];

  final int? userId;
  final int? id;
  final String? title;
  List<PhotosResponse>? photos;

  factory AlbumsResponse.fromJson(Map<String, dynamic> json) => AlbumsResponse(
    userId: json["userId"],
    id: json["id"],
    title: json["title"],
  );
}
