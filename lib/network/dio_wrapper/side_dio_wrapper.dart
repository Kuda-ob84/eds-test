import 'package:dio/dio.dart';
import 'package:eclipse_test/generated/l10n.dart';

class SideDioWrapper {
  final Dio _dio = Dio();

  Dio get dio => _dio;

  SideDioWrapper() {
    _dio.interceptors.add(LogInterceptor(requestBody: true));
    _dio.options.connectTimeout = 40000;
    _dio.options.receiveTimeout = 40000;
    _dio.options.sendTimeout = 40000;
    _dio.options.responseType = ResponseType.json;
  }
}


extension DioErrorUtils on Object {
  String get dioErrorMessage {
    var output = S.current.error_general;
    if (this is! DioError) return output;
    final error = this as DioError;
    final List? errorsList = error.response?.data['errors'];
    if (errorsList == null) return output;
    if (errorsList.isNotEmpty) {
      output = errorsList.first.toString();
    }
    return output;
  }

  int? get dioErrorStatusCode {
    int? output;
    if (this is! DioError) return output;
    return (this as DioError).response?.statusCode;
  }
}
