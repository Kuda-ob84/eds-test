import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:eclipse_test/network/dio_wrapper/dio_wrapper.dart';
import 'package:eclipse_test/network/dio_wrapper/side_dio_wrapper.dart';
import 'package:eclipse_test/network/models/dto_models/encodable.dart';
import 'package:eclipse_test/network/models/dto_models/request/comment.dart';
import 'package:eclipse_test/network/models/dto_models/response/albums.dart';
import 'package:eclipse_test/network/models/dto_models/response/comments.dart';
import 'package:eclipse_test/network/models/dto_models/response/photos.dart';
import 'package:eclipse_test/network/models/dto_models/response/posts.dart';
import 'package:eclipse_test/network/models/dto_models/response/users.dart';
import 'package:eclipse_test/network/repository/hive_repository.dart';
import 'package:path_provider/path_provider.dart';

class NetworkService {
  late final DioWrapper _dioWrapper;
  late final HiveRepository _hiveRepository;
  final SideDioWrapper _sideDioWrapper = SideDioWrapper();

  void init(DioWrapper dioService, HiveRepository hiveRepository) {
    _dioWrapper = dioService;
    _hiveRepository = hiveRepository;
  }

  Future<List<UsersResponse>> getProfiles() async {
    String? profiles = _hiveRepository.getUsersResponse();
    var response;
    if (profiles!.isEmpty) {
      response = await _dioWrapper.sendRequest(
        path: "users",
        method: NetworkMethod.get,
      );
      await _hiveRepository.setUsersResponse(json.encode(response.data));
      return usersResponseFromJson(json.encode(response.data));
    } else {
      return usersResponseFromJson(profiles);
    }
  }

  Future<List<PostsResponse>> getPosts() async {
    String? posts = _hiveRepository.getPostsResponse();
    var response;
    if (posts!.isEmpty) {
      response = await _dioWrapper.sendRequest(
        path: "posts",
        method: NetworkMethod.get,
      );
      await _hiveRepository.setPostsResponse(json.encode(response.data));
      return postsResponseFromJson(json.encode(response.data));
    } else {
      return postsResponseFromJson(posts);
    }
  }

  Future<List<PostsResponse>> getUserPosts(int userId) async {
    var response = await _dioWrapper.sendRequest(
      path: "posts",
      queryParameters: {
        "userId": userId,
      },
      method: NetworkMethod.get,
    );
    return postsResponseFromJson(json.encode(response.data));
  }

  Future<List<CommentsResponse>> getComments() async {
    String? comments = _hiveRepository.getCommentsResponse();
    var response;
    if (comments!.isEmpty) {
      response = await _dioWrapper.sendRequest(
        path: "comments",
        method: NetworkMethod.get,
      );
      await _hiveRepository.setCommentsResponse(json.encode(response.data));
      return commentsResponseFromJson(json.encode(response.data));
    } else {
      return commentsResponseFromJson(comments);
    }
  }

  Future<List<AlbumsResponse>> getAlbums(int userId) async {
    var response = await _dioWrapper.sendRequest(
      path: "albums",
      queryParameters: {
        "userId": userId,
      },
      method: NetworkMethod.get,
    );
    return albumsResponseFromJson(json.encode(response.data));
  }

  Future<List<PhotosResponse>> getPhotos(int albumId) async {
    var response = await _dioWrapper.sendRequest(
      path: "photos",
      queryParameters: {
        "albumId": albumId,
      },
      method: NetworkMethod.get,
    );
    return photosResponseFromJson(json.encode(response.data));
  }

  Future<bool> addComment(int postId, CommentRequest commentRequest) async {
    var response = await _dioWrapper.sendRequest(
      path: "posts/${postId}/comments",
      method: NetworkMethod.post,
      request: SimpleEncodable(commentRequest.toJson()),
    );
    return response.statusCode == 200;
  }
}
