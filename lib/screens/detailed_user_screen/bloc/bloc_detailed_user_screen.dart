import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:eclipse_test/network/dio_wrapper/side_dio_wrapper.dart';
import 'package:eclipse_test/network/models/dto_models/response/albums.dart';
import 'package:eclipse_test/network/models/dto_models/response/error.dart';
import 'package:eclipse_test/network/models/dto_models/response/posts.dart';
import 'package:eclipse_test/network/models/dto_models/response/users.dart';
import 'package:eclipse_test/network/repository/global_repository.dart';
import 'package:meta/meta.dart';

part 'events.dart';

part 'states.dart';

part 'parts/read.dart';

class BlocDetailedUserScreen
    extends Bloc<EventBlocDetailedUserScreen, StateBlocDetailedUserScreen> {
  BlocDetailedUserScreen({
    required this.repository,
    required this.userInfo,
  }) : super(StateDetailedUserScreenInitial()) {
    on<EventInitialDetailedUserScreen>(_read);
  }

  final GlobalRepository repository;
  final UsersResponse userInfo;
}
