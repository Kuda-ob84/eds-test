part of 'bloc_detailed_user_screen.dart';

abstract class StateBlocDetailedUserScreen {}

class StateDetailedUserScreenInitial extends StateBlocDetailedUserScreen {}

class StateErrorAlbums extends StateBlocDetailedUserScreen {
  final AppError error;

  StateErrorAlbums({
    required this.error,
  });
}

class StateLoadDetailedUserData extends StateBlocDetailedUserScreen {
  final List<PostsResponse> posts;
  final List<AlbumsResponse> albums;

  StateLoadDetailedUserData({
    required this.posts,
    required this.albums,
  });
}
