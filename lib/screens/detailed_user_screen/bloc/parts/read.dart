part of "../bloc_detailed_user_screen.dart";

extension Read on BlocDetailedUserScreen {
  Future<void> _read(
    EventInitialDetailedUserScreen event,
    Emitter<StateBlocDetailedUserScreen> emit,
  ) async {
    try {
      var albums = await repository.getAlbums(userInfo.id!);
      for (int i = 0; i < albums.length; i++) {
        var photos = await repository.getPhotos(albums[i].id!);
        albums[i].photos!.addAll(photos);
      }
      var posts = await repository.getUserPosts(userInfo.id!);
      emit(StateLoadDetailedUserData(
        posts: posts,
        albums: albums,
      ));
      print(albums);
    } catch (e) {
      emit(StateErrorAlbums(
        error: AppError(
          message: e.dioErrorMessage,
          code: e.dioErrorStatusCode,
        ),
      ));
    }
  }
}
