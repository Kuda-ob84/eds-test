import 'package:eclipse_test/constants/app_colors.dart';
import 'package:eclipse_test/constants/app_text_styles.dart';
import 'package:eclipse_test/generated/l10n.dart';
import 'package:eclipse_test/network/models/dto_models/response/users.dart';
import 'package:eclipse_test/network/repository/global_repository.dart';
import 'package:eclipse_test/screens/albums_screen/ui/albums_screen.dart';
import 'package:eclipse_test/screens/bottom_navigation_bar/cubit/bottom_nav_bar_cubit.dart';
import 'package:eclipse_test/screens/detailed_user_screen/bloc/bloc_detailed_user_screen.dart';
import 'package:eclipse_test/screens/posts_screen/bloc/bloc_posts_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DetailedUserScreen extends StatelessWidget {
  final UsersResponse userInfo;

  const DetailedUserScreen({
    Key? key,
    required this.userInfo,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BlocDetailedUserScreen>(
      create: (context) => BlocDetailedUserScreen(
        repository: context.read<GlobalRepository>(),
        userInfo: userInfo,
      )..add(EventInitialDetailedUserScreen()),
      child: Scaffold(
        backgroundColor: AppColors.backgroundShade,
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
          ),
          elevation: 0,
          backgroundColor: AppColors.backgroundShade,
          centerTitle: true,
          title: Text(
            userInfo.username!,
            style: const TextStyle(
                color: Colors.black, fontSize: 18, fontWeight: FontWeight.w500),
          ),
        ),
        body: BlocConsumer<BlocDetailedUserScreen, StateBlocDetailedUserScreen>(
          listener: (context, state) {},
          builder: (context, state) {
            if (state is StateLoadDetailedUserData) {
              return SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 18.0, right: 18.0, bottom: 18.0),
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                      vertical: 20,
                      horizontal: 16,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Center(
                          child: Image.asset(
                            "assets/images/png/no_photo.png",
                            width: 105,
                            height: 105,
                          ),
                        ),
                        const SizedBox(
                          height: 18,
                        ),
                        Column(
                          children: [
                            _BuildUserInfo(
                              title: "Name",
                              description:
                                  userInfo.name ?? S.of(context).no_data,
                            ),
                            _BuildUserInfo(
                              title: "Username",
                              description:
                                  userInfo.username ?? S.of(context).no_data,
                            ),
                            _BuildUserInfo(
                              title: "Email",
                              description:
                                  userInfo.email ?? S.of(context).no_data,
                            ),
                            _BuildUserInfo(
                              title: "Phone",
                              description:
                                  userInfo.phone ?? S.of(context).no_data,
                            ),
                            _BuildUserInfo(
                              title: "Website",
                              description:
                                  userInfo.website ?? S.of(context).no_data,
                            ),
                            Center(
                              child: Text(
                                "Company",
                                style: AppTextStyles.paragraph
                                    .copyWith(color: AppColors.dark),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              padding: const EdgeInsets.symmetric(
                                vertical: 10,
                                horizontal: 10,
                              ),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  border: Border.all(color: AppColors.dark)),
                              child: Column(
                                children: [
                                  _BuildUserInfo(
                                    title: "Company name",
                                    description: userInfo.company!.name ??
                                        S.of(context).no_data,
                                  ),
                                  _BuildUserInfo(
                                    title: "Company catch phrase",
                                    description:
                                        userInfo.company!.catchPhrase ??
                                            S.of(context).no_data,
                                  ),
                                  _BuildUserInfo(
                                    title: "Company Bs",
                                    description: userInfo.company!.bs ??
                                        S.of(context).no_data,
                                    isLast: true,
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Center(
                              child: Text(
                                "Address",
                                style: AppTextStyles.paragraph
                                    .copyWith(color: AppColors.dark),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              padding: const EdgeInsets.symmetric(
                                vertical: 10,
                                horizontal: 10,
                              ),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  border: Border.all(color: AppColors.dark)),
                              child: Column(
                                children: [
                                  _BuildUserInfo(
                                    title: "Street",
                                    description: userInfo.address!.street ??
                                        S.of(context).no_data,
                                  ),
                                  _BuildUserInfo(
                                    title: "Suite",
                                    description: userInfo.address!.suite ??
                                        S.of(context).no_data,
                                  ),
                                  _BuildUserInfo(
                                    title: "Zipcode",
                                    description: userInfo.address!.zipcode ??
                                        S.of(context).no_data,
                                    isLast: true,
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 28,
                            ),
                            InkWell(
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "${S.of(context).posts}: ${state.posts.length}",
                                      style: AppTextStyles.labelMedium.copyWith(
                                          color: AppColors.blue, fontSize: 16),
                                    ),
                                    const Icon(Icons.chevron_right),
                                  ],
                                ),
                              ),
                              onTap: () {
                                context
                                    .read<BlocPostsScreen>()
                                    .add(EventsSearchPostByUser(
                                      name: userInfo.name!,
                                      isFromAnotherPage: true,
                                    ));
                                Navigator.of(context).pop();
                                context
                                    .read<BottomNavBarCubit>()
                                    .changeCurrentPage(1);
                              },
                            ),
                            const SizedBox(
                              height: 18,
                            ),
                            InkWell(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "${S.of(context).albums}: ${state.albums.length}",
                                    style: AppTextStyles.labelMedium.copyWith(
                                        color: AppColors.blue, fontSize: 16),
                                  ),
                                  const Icon(Icons.chevron_right),
                                ],
                              ),
                              onTap: () {

                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => AlbumsScreen(
                                          albums: state.albums,
                                        )));
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}

class _BuildUserInfo extends StatelessWidget {
  final String title;
  final String description;
  final bool isLast;

  const _BuildUserInfo({
    Key? key,
    required this.title,
    required this.description,
    this.isLast = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: isLast ? 0 : 18.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(title),
          const SizedBox(
            width: 38,
          ),
          Flexible(
              child: Text(
            description,
            overflow: TextOverflow.fade,
            textAlign: TextAlign.right,
          )),
        ],
      ),
    );
  }
}
