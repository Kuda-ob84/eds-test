import 'package:eclipse_test/generated/l10n.dart';
import 'package:eclipse_test/network/models/dto_models/response/users.dart';
import 'package:eclipse_test/widgets/app_list_tile.dart';
import 'package:flutter/material.dart';

class AppUserInfoPreview extends StatelessWidget {
  const AppUserInfoPreview({
    Key? key,
    required this.userInfo,
    required this.isLast,
    required this.onTap,
  }) : super(key: key);

  final UsersResponse userInfo;
  final bool isLast;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return AppListTile(
      leading: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 28,
        backgroundImage: AssetImage("assets/images/png/no_photo.png"),
      ),
      title: userInfo.name ?? S.of(context).no_data,
      subtitle: userInfo.username ?? S.of(context).no_data,
      onTap: onTap,
    );
  }
}
