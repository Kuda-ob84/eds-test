import 'package:eclipse_test/constants/app_colors.dart';
import 'package:eclipse_test/generated/l10n.dart';
import 'package:eclipse_test/network/repository/global_repository.dart';
import 'package:eclipse_test/screens/detailed_user_screen/ui/detailed_user_screen.dart';
import 'package:eclipse_test/screens/users_preview_screen/bloc/bloc_users_preview.dart';
import 'package:eclipse_test/screens/users_preview_screen/ui/widgets/user_info.dart';
import 'package:eclipse_test/widgets/app_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UsersPreviewScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundShade,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: AppColors.backgroundShade,
        centerTitle: true,
        title: Text(
          S.of(context).users,
          style: const TextStyle(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w500),
        ),
      ),
      body: BlocConsumer<BlocUsersPreview, StateBlocUsersPreview>(
        builder: (context, state) {
          if (state is StateLoadUsersPreview) {
            return Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 0.0,
                horizontal: 16,
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Column(
                      children: state.usersInfo.map((e) {
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 18.0),
                          child: AppUserInfoPreview(
                            userInfo: e,
                            onTap: () {
                              Navigator.of(context, rootNavigator: true).push(
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          DetailedUserScreen(userInfo: e)));
                            },
                            isLast: false,
                          ),
                        );
                      }).toList(),
                    ),
                    SizedBox(
                      height: 75,
                    )
                  ],
                ),
              ),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
          ;
        },
        buildWhen: (p, c) => c is StateLoadUsersPreview,
        listener: (context, state) {
          if (state is StateUsersPreviewError) {
            showAppDialog(
              context,
              body: state.error.message,
            );
          }
        },
      ),
    );
  }
}
