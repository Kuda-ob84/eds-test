import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:eclipse_test/network/dio_wrapper/side_dio_wrapper.dart';
import 'package:eclipse_test/network/models/dto_models/response/error.dart';
import 'package:eclipse_test/network/models/dto_models/response/users.dart';
import 'package:eclipse_test/network/repository/global_repository.dart';
import 'package:meta/meta.dart';

part 'events.dart';

part 'states.dart';
part 'parts/read.dart';
part 'extensions.dart';

class BlocUsersPreview
    extends Bloc<EventUsersPreview, StateBlocUsersPreview> {
  final GlobalRepository repository;

  BlocUsersPreview({
    required this.repository,
  }) : super(StateUsersPreviewInitial()) {
    on<EventInitialUsersPreview>(_read);
  }
}
