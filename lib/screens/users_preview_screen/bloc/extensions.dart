part of 'bloc_users_preview.dart';

extension BlocOnboardingUtils on BlocUsersPreview{
  List<UsersResponse> get users{
    if(state is StateLoadUsersPreview){
      return (state as StateLoadUsersPreview).usersInfo;
    }
    return [];
  }
}