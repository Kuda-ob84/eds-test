part of 'bloc_users_preview.dart';

abstract class StateBlocUsersPreview {}

class StateUsersPreviewInitial extends StateBlocUsersPreview {}

class StateLoadUsersPreview extends StateBlocUsersPreview {
  final List<UsersResponse> usersInfo;

  StateLoadUsersPreview({
    required this.usersInfo,
  });
}

class StateUsersPreviewError extends StateBlocUsersPreview {
  final AppError error;

  StateUsersPreviewError({
    required this.error,
  });
}
