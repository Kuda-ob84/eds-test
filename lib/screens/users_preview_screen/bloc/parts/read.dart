part of '../bloc_users_preview.dart';

extension Read on BlocUsersPreview {
  Future<void> _read(
    EventInitialUsersPreview event,
    Emitter<StateBlocUsersPreview> emit,
  ) async {
    try {
      List<UsersResponse> usersInfo = await repository.getProfiles();
      emit(StateLoadUsersPreview(usersInfo: usersInfo));
    } catch (e) {
      emit(
        StateUsersPreviewError(
          error: AppError(
            message: e.dioErrorMessage,
            code: e.dioErrorStatusCode,
          ),
        ),
      );
      print(e.dioErrorMessage);
    }
  }
}
