part of 'bloc_detailed_post_screen.dart';

@immutable
abstract class StateBlocDetailedPost {}

class StateLoadingPost extends StateBlocDetailedPost {}
class StateSuccessfullyAdded extends StateBlocDetailedPost {}

class StateErrorPost extends StateBlocDetailedPost {
  final AppError error;

  StateErrorPost({
    required this.error,
  });
}

class StateLoadPost extends StateBlocDetailedPost {
  final PostsResponse post;
  final List<CommentsResponse> comments;

  StateLoadPost({
    required this.post,
    required this.comments,
  });
}
