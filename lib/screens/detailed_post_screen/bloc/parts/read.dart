part of '../bloc_detailed_post_screen.dart';

extension Read on BlocDetailedPostScreen {
  Future<void> _read(
    EventInitialDetailedPost event,
    Emitter<StateBlocDetailedPost> emit,
  ) async {
    try {
      var comments = await repository.getComments();
      comments = comments.where((element) => element.postId == post.id!).toList();
      emit(StateLoadPost(post: post, comments: comments));
    } catch (e) {
      emit(StateErrorPost(
        error: AppError(
          message: e.dioErrorMessage,
          code: e.dioErrorStatusCode,
        ),
      ));
    }
  }
}
