part of '../bloc_detailed_post_screen.dart';

extension Add on BlocDetailedPostScreen {
  Future<void> _addComment(
      EventAddCommentToPost event, Emitter<StateBlocDetailedPost> emit) async {
    try {
      await repository.addComment(event.id, event.commentRequest);
      emit(StateSuccessfullyAdded());
    } catch (e) {
      emit(StateErrorPost(
        error: AppError(
          message: e.dioErrorMessage,
          code: e.dioErrorStatusCode,
        ),
      ));
    }
  }
}
