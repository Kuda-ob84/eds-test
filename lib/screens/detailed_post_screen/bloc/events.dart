part of 'bloc_detailed_post_screen.dart';

@immutable
abstract class EventDetailedPostScreen {}

class EventInitialDetailedPost extends EventDetailedPostScreen {}

class EventAddCommentToPost extends EventDetailedPostScreen {
  final CommentRequest commentRequest;
  final int id;

  EventAddCommentToPost({required this.commentRequest, required this.id});
}
