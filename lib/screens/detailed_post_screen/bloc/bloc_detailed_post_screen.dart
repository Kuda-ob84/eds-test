import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:eclipse_test/network/dio_wrapper/side_dio_wrapper.dart';
import 'package:eclipse_test/network/models/dto_models/request/comment.dart';
import 'package:eclipse_test/network/models/dto_models/response/comments.dart';
import 'package:eclipse_test/network/models/dto_models/response/error.dart';
import 'package:eclipse_test/network/models/dto_models/response/posts.dart';
import 'package:eclipse_test/network/repository/global_repository.dart';
import 'package:meta/meta.dart';

part 'events.dart';

part 'states.dart';

part 'parts/read.dart';
part 'parts/add_comment.dart';

class BlocDetailedPostScreen
    extends Bloc<EventDetailedPostScreen, StateBlocDetailedPost> {
  BlocDetailedPostScreen({
    required this.post,
    required this.repository,
  }) : super(StateLoadingPost()) {
    on<EventInitialDetailedPost>(_read);
    on<EventAddCommentToPost>(_addComment);
  }

  final PostsResponse post;
  final GlobalRepository repository;
}
