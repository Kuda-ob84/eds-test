import 'package:cached_network_image/cached_network_image.dart';
import 'package:eclipse_test/constants/app_colors.dart';
import 'package:eclipse_test/constants/app_text_styles.dart';
import 'package:eclipse_test/generated/l10n.dart';
import 'package:eclipse_test/network/models/dto_models/request/comment.dart';
import 'package:eclipse_test/network/models/dto_models/response/comments.dart';
import 'package:eclipse_test/network/models/dto_models/response/posts.dart';
import 'package:eclipse_test/network/repository/global_repository.dart';
import 'package:eclipse_test/screens/detailed_post_screen/bloc/bloc_detailed_post_screen.dart';
import 'package:eclipse_test/screens/detailed_user_screen/bloc/bloc_detailed_user_screen.dart';
import 'package:eclipse_test/widgets/app_bottom_dialog.dart';
import 'package:eclipse_test/widgets/app_button.dart';
import 'package:eclipse_test/widgets/app_dialog.dart';
import 'package:eclipse_test/widgets/main_text_field/app_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

part 'comment_bottom_dialog.dart';


class DetailedPostScreen extends StatelessWidget {
  final PostsResponse post;

  const DetailedPostScreen({
    Key? key,
    required this.post,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BlocDetailedPostScreen>(
      create: (context) => BlocDetailedPostScreen(
        post: post,
        repository: context.read<GlobalRepository>(),
      )..add(EventInitialDetailedPost()),
      child: Scaffold(
        backgroundColor: AppColors.backgroundShade,
        appBar: const _BuildAppBar(),
        body: BlocConsumer<BlocDetailedPostScreen, StateBlocDetailedPost>(
          builder: (context, state) {
            if (state is StateLoadPost) {
              return SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(
                    bottom: 18.0,
                    left: 18,
                    right: 18,
                  ),
                  child: Container(
                    padding:
                        const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                    decoration: BoxDecoration(
                        color: AppColors.background,
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: const [
                          BoxShadow(
                            color: Color.fromRGBO(16, 51, 115, 0.2),
                            blurRadius: 10,
                            offset: Offset(0, 0),
                          )
                        ]),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          children: [
                            Image.asset(
                              "assets/images/png/no_photo.png",
                              width: 36,
                              height: 36,
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(post.user?.name ??
                                    S.of(context).user_not_found),
                                const SizedBox(
                                  height: 5,
                                ),
                                post.date!.day == DateTime.now().day &&
                                        post.date!.month ==
                                            DateTime.now().month &&
                                        post.date!.year == DateTime.now().year
                                    ? Text(
                                        "Today at ${DateFormat('kk:mm').format(post.date!)}")
                                    : post.date!.day ==
                                                DateTime.now().day - 1 &&
                                            post.date!.month ==
                                                DateTime.now().month &&
                                            post.date!.year ==
                                                DateTime.now().year
                                        ? Text(
                                            "Yesterday at ${DateFormat('kk:mm').format(post.date!)}")
                                        : Text(DateFormat('dd MMMM kk:mm')
                                            .format(post.date!)),
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Center(
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(15),
                            child: CachedNetworkImage(
                              imageUrl: post.imageUrl!,
                              fit: BoxFit.cover,
                              width: MediaQuery.of(context).size.width,
                              height: 220,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 18,
                        ),
                        const Divider(
                          color: AppColors.primary2,
                          height: 1,
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Text(
                          "${state.comments.length} ${S.of(context).comments}",
                          style: AppTextStyles.pMedium
                              .copyWith(color: AppColors.dark),
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        _BuildCommentsSection(
                          comments: state.comments,
                        ),
                        Center(
                            child: AppElevatedButton(
                          label: "Add comment",
                          onPressed: () {
                            showWriteCommentBottomSheet(context, context.read<BlocDetailedPostScreen>(), post.id!);
                          },
                        )),
                      ],
                    ),
                  ),
                ),
              );
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
          listener: (context, state) {
            if(state is StateErrorPost){
              showAppDialog(context, body: state.error.message);
            }
          },
          buildWhen: (p, c) => c is StateLoadPost,
        ),
      ),
    );
  }
}

class _BuildAppBar extends StatelessWidget implements PreferredSizeWidget {
  const _BuildAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: AppColors.backgroundShade,
      centerTitle: true,
      leading: IconButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        icon: const Icon(
          Icons.arrow_back_ios,
          color: Colors.black,
        ),
      ),
      title: Text(
        S.of(context).post,
        style: const TextStyle(
            color: Colors.black, fontSize: 18, fontWeight: FontWeight.w500),
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => const Size.fromHeight(52);
}

class _BuildCommentsSection extends StatelessWidget {
  final List<CommentsResponse> comments;

  const _BuildCommentsSection({
    Key? key,
    required this.comments,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: comments.map((e) {
        return Padding(
          padding: const EdgeInsets.only(bottom: 38.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  const CircleAvatar(
                    backgroundColor: Colors.transparent,
                    radius: 28,
                    backgroundImage:
                        AssetImage("assets/images/png/no_photo.png"),
                  ),
                  const SizedBox(
                    width: 9,
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          e.name ?? S.of(context).no_data,
                          overflow: TextOverflow.fade,
                          style: AppTextStyles.caption
                              .copyWith(color: AppColors.dark.withOpacity(0.5)),
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Text(
                          e.email ?? S.of(context).no_data,
                          overflow: TextOverflow.fade,
                          style: AppTextStyles.caption
                              .copyWith(color: AppColors.dark.withOpacity(0.5)),
                        )
                      ],
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 8,
              ),
              Text(e.body ?? S.of(context).no_data),
            ],
          ),
        );
      }).toList(),
    );
  }
}
