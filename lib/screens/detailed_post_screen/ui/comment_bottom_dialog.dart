part of 'detailed_post_screen.dart';

void showWriteCommentBottomSheet(
  BuildContext context,
  BlocDetailedPostScreen bloc,
  int id,
) {
  showAppBottomSheet(context,
      initialChildSize: 0.45,
      useRootNavigator: true,
      child: _BuildWriteCommentField(
        bloc: bloc,
        id: id,
      ));
}

class _BuildWriteCommentField extends StatefulWidget {
  final BlocDetailedPostScreen bloc;
  final int id;

  const _BuildWriteCommentField({
    Key? key,
    required this.bloc,
    required this.id,
  }) : super(key: key);

  @override
  _BuildWriteCommentFieldState createState() => _BuildWriteCommentFieldState();
}

class _BuildWriteCommentFieldState extends State<_BuildWriteCommentField> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: widget.bloc,
      child: BlocConsumer<BlocDetailedPostScreen, StateBlocDetailedPost>(
        listener: (context, state)  {
          if(state is StateErrorPost){
            showAppDialog(context, body: state.error.message);
          }
          if(state is StateSuccessfullyAdded){
            Navigator.of(context).pop();
            showAppDialog(context, title: "Successfully added");

          }
        },
        builder: (context, state) {
          return Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                AppTextField(
                  hintText: S.of(context).name,
                  controller: nameController,
                ),
                const SizedBox(
                  height: 15,
                ),
                AppTextField(
                  hintText: S.of(context).email,
                  controller: emailController,
                ),
                const SizedBox(
                  height: 15,
                ),
                AppTextField(
                  hintText: S.of(context).description,
                  controller: descriptionController,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: AppElevatedButton(
                    label: S.of(context).add,
                    onPressed: () {
                      context.read<BlocDetailedPostScreen>().add(
                          EventAddCommentToPost(
                              commentRequest: CommentRequest(
                                  name: nameController.text,
                                  email: emailController.text,
                                  description: descriptionController.text),
                              id: widget.id));
                    },
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
