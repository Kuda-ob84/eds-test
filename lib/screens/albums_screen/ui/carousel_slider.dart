part of 'albums_screen.dart';

class CarouselPhotoSlider extends StatefulWidget {
  final List<PhotosResponse> images;

  const CarouselPhotoSlider({Key? key, required this.images}) : super(key: key);

  @override
  _CarouselPhotoSliderState createState() => _CarouselPhotoSliderState();
}

class _CarouselPhotoSliderState extends State<CarouselPhotoSlider> {
  late int photoIndex;
  CarouselController carouselController = CarouselController();
  ItemScrollController itemScrollController = ItemScrollController();
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    photoIndex = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double screenSize = MediaQuery.of(context).size.width;
    double imageSize = screenSize < 400 ? 80 : 90;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Padding(
          padding: const EdgeInsets.only(top: 24, bottom: 50),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 28, right: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                        onPressed: () =>
                            Navigator.of(context, rootNavigator: true).pop(),
                        icon: const Icon(
                          Icons.close,
                          color: Colors.white,
                          size: 35,
                        )),
                    Text(
                      "${photoIndex + 1}/${widget.images.length}",
                      style: AppTextStyles.s16w500.copyWith(
                        color: AppColors.background,
                      ),
                    )
                  ],
                ),
              ),
              CarouselSlider.builder(
                carouselController: carouselController,
                itemCount: widget.images.length,
                itemBuilder: (BuildContext context, int index, int realIndex) {
                  return ClipRRect(
                    child: CachedNetworkImage(
                      height: 277,
                      width: double.infinity,
                      imageUrl: widget.images[index].url!,
                      fit: BoxFit.cover,
                      placeholder: (ctx, url) => Image.asset(
                        'assets/images/png/placeholder_orange.png',
                        height: 196,
                        fit: BoxFit.cover,
                        width: MediaQuery.of(context).size.width,
                      ),
                    ),
                  );
                },
                options: CarouselOptions(
                  enableInfiniteScroll: false,
                  onPageChanged: (i, CarouselPageChangedReason reason) {
                    if (i > 1 && i < widget.images.length - 1) {
                      itemScrollController.scrollTo(
                          index: i,
                          alignment: screenSize > 400 &&
                                  i == (widget.images.length - 2)
                              ? 0.45
                              : 0.35,
                          duration: const Duration(seconds: 1),
                          curve: Curves.easeInOutCubic);
                    } else if (i == 1 && photoIndex == 2) {
                      itemScrollController.scrollTo(
                          index: 0,
                          duration: const Duration(seconds: 1),
                          curve: Curves.easeInOutCubic);
                    }
                    setState(() {
                      photoIndex = i;
                    });
                  },
                  enlargeCenterPage: true,
                  viewportFraction: 0.9,
                  aspectRatio: 2.0,
                  initialPage: 0,
                  scrollDirection: Axis.horizontal,
                ),
              ),
              SizedBox(
                height: imageSize,
                child: ScrollablePositionedList.builder(
                    itemScrollController: itemScrollController,
                    initialScrollIndex: photoIndex,
                    scrollDirection: Axis.horizontal,
                    itemCount: widget.images.length,
                    itemBuilder: (context, index) {
                      bool isSelected = index == photoIndex;
                      return _BuildImage(
                          imageLink: widget.images[index].thumbnailUrl!,
                          onTap: () {
                            carouselController.jumpToPage(index);
                          },
                          imageSize: imageSize,
                          isSelected: isSelected);
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class _BuildImage extends StatelessWidget {
  final String imageLink;
  final VoidCallback onTap;
  final bool isSelected;
  final double imageSize;

  const _BuildImage(
      {Key? key,
      required this.imageLink,
      required this.onTap,
      required this.isSelected,
      required this.imageSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: GestureDetector(
        onTap: onTap,
        child: CachedNetworkImage(
          imageUrl: imageLink,
          imageBuilder: (context, imageProvider) => Container(
            width: imageSize,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              border: Border.all(
                  color: isSelected ? Colors.white : Colors.transparent,
                  width: 2),
              image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
            ),
          ),
          placeholder: (ctx, url) {
            return Image.asset(
              'assets/images/png/placeholder_orange.png',
              fit: BoxFit.cover,
              width: imageSize,
            );
          },
          errorWidget: (context, url, error) => const Icon(Icons.error),
        ),
      ),
    );
  }
}
