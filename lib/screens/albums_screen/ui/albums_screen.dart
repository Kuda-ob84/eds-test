import 'package:eclipse_test/constants/app_colors.dart';
import 'package:eclipse_test/generated/l10n.dart';
import 'package:eclipse_test/network/models/dto_models/response/albums.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:eclipse_test/constants/app_colors.dart';
import 'package:eclipse_test/constants/app_text_styles.dart';
import 'package:eclipse_test/network/models/dto_models/response/photos.dart';
import 'package:flutter/material.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

part 'carousel_slider.dart';

class AlbumsScreen extends StatefulWidget {
  final List<AlbumsResponse> albums;

  const AlbumsScreen({
    Key? key,
    required this.albums,
  }) : super(key: key);

  @override
  _AlbumsScreenState createState() => _AlbumsScreenState();
}

class _AlbumsScreenState extends State<AlbumsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.backgroundShade,
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
          ),
          elevation: 0,
          backgroundColor: AppColors.backgroundShade,
          centerTitle: true,
          title: Text(
            S.of(context).albums,
            style: const TextStyle(
                color: Colors.black, fontSize: 18, fontWeight: FontWeight.w500),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
              children: widget.albums.map((e) {
            return Padding(
              padding: const EdgeInsets.only(
                bottom: 10.0,
                left: 8,
                right: 8,
              ),
              child: Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 18,
                ),
                decoration: BoxDecoration(
                    color: AppColors.background,
                    borderRadius: BorderRadius.circular(15)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0, right: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                              child: Text(
                            e.title ?? S.of(context).no_data,
                            overflow: TextOverflow.fade,
                          )),
                          GestureDetector(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => CarouselPhotoSlider(
                                          images: e.photos!,
                                        )));
                              },
                              child: const Icon(
                                Icons.chevron_right,
                                color: AppColors.dark,
                              )),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 18,
                    ),
                    SizedBox(
                      height: 95,
                      child: ScrollablePositionedList.builder(
                          initialScrollIndex: 0,
                          scrollDirection: Axis.horizontal,
                          itemCount: e.photos!.length,
                          itemBuilder: (context, index) {
                            return _BuildImage(
                                imageLink: e.photos![index].thumbnailUrl!,
                                onTap: () {},
                                imageSize: 95,
                                isSelected: false);
                          }),
                    ),
                  ],
                ),
              ),
            );
          }).toList()),
        ));
  }
}
