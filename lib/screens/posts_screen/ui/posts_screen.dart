import 'package:eclipse_test/constants/app_colors.dart';
import 'package:eclipse_test/generated/l10n.dart';
import 'package:eclipse_test/screens/detailed_post_screen/ui/detailed_post_screen.dart';
import 'package:eclipse_test/screens/posts_screen/bloc/bloc_posts_screen.dart';
import 'package:eclipse_test/screens/posts_screen/ui/widget/app_post_preview.dart';
import 'package:eclipse_test/widgets/app_dialog.dart';
import 'package:eclipse_test/widgets/main_text_field/app_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PostsScreen extends StatelessWidget {
  final int? userId;

  const PostsScreen({Key? key, this.userId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.backgroundShade,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: AppColors.backgroundShade,
          centerTitle: true,
          title: Text(
            S.of(context).posts,
            style: const TextStyle(
                color: Colors.black, fontSize: 18, fontWeight: FontWeight.w500),
          ),
        ),
        body: BlocConsumer<BlocPostsScreen, StateBlocPosts>(
          builder: (context, state) {
            if (state is StateLoadPosts) {
              return Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16.0,
                      vertical: 8,
                    ),
                    child: AppTextField(
                      hintText: S.current.enter_username,
                      controller: state.controller,
                      onChanged: (value) {
                        context
                            .read<BlocPostsScreen>()
                            .add(EventsSearchPostByUser(name: value));
                      },
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: state.posts.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.only(
                              bottom: 8.0,
                              left: 16,
                              right: 16,
                            ),
                            child: AppPostPreview(
                              post: state.posts[index],
                              onTap: () {
                                Navigator.of(context, rootNavigator: true).push(
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            DetailedPostScreen(
                                                post: state.posts[index])));
                              },
                            ),
                          );
                        }),
                  ),
                ],
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
          listener: (context, state) {
            if(state is StatePostsScreenError){
              showAppDialog(context, body: state.error.message);
            }
          },
          buildWhen: (p, c) => c is StateLoadPosts,
        ));
  }
}
