import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:eclipse_test/constants/app_colors.dart';
import 'package:eclipse_test/constants/app_text_styles.dart';
import 'package:eclipse_test/generated/l10n.dart';
import 'package:eclipse_test/network/models/dto_models/response/posts.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AppPostPreview extends StatelessWidget {
  const AppPostPreview({
    Key? key,
    required this.post,
    required this.onTap,
  }) : super(key: key);

  final PostsResponse post;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 16,
          horizontal: 22,
        ),
        decoration: BoxDecoration(
            color: AppColors.background,
            borderRadius: BorderRadius.circular(10),
            boxShadow: const [
              BoxShadow(
                color: Color.fromRGBO(16, 51, 115, 0.2),
                blurRadius: 10,
                offset: Offset(0, 0),
              )
            ]),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Image.asset(
                  "assets/images/png/no_photo.png",
                  width: 36,
                  height: 36,
                ),
                const SizedBox(
                  width: 8,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(post.user?.name ?? S.of(context).user_not_found,
                        style: AppTextStyles.caption
                            .copyWith(color: AppColors.dark.withOpacity(0.5))),
                    const SizedBox(
                      height: 5,
                    ),
                    post.date!.day == DateTime.now().day &&
                            post.date!.month == DateTime.now().month &&
                            post.date!.year == DateTime.now().year
                        ? Text(
                            "Today at ${DateFormat('kk:mm').format(post.date!)}",
                            style: AppTextStyles.caption.copyWith(
                                color: AppColors.dark.withOpacity(0.5)))
                        : post.date!.day == DateTime.now().day - 1 &&
                                post.date!.month == DateTime.now().month &&
                                post.date!.year == DateTime.now().year
                            ? Text(
                                "Yesterday at ${DateFormat('kk:mm').format(post.date!)}",
                                style: AppTextStyles.caption.copyWith(
                                    color: AppColors.dark.withOpacity(0.5)))
                            : Text(
                                DateFormat('dd MMMM kk:mm').format(post.date!),
                                style: AppTextStyles.caption.copyWith(
                                    color: AppColors.dark.withOpacity(0.5))),
                  ],
                ),
              ],
            ),
            const SizedBox(
              height: 8,
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              post.title ?? S.of(context).no_data,
            ),
          ],
        ),
      ),
    );
  }
}
