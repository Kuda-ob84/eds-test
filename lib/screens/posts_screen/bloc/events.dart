part of 'bloc_posts_screen.dart';

@immutable
abstract class EventBlocPosts {}

class EventInitialPostsScreen extends EventBlocPosts {
  final int? userId;

  EventInitialPostsScreen({
    this.userId,
  });
}

class EventsSearchPostByUser extends EventBlocPosts {
  final String name;
  final bool isFromAnotherPage;

  EventsSearchPostByUser({
    required this.name,
    this.isFromAnotherPage = false,
  });
}
