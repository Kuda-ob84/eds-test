import 'dart:async';
import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:eclipse_test/network/dio_wrapper/side_dio_wrapper.dart';
import 'package:eclipse_test/network/models/dto_models/response/error.dart';
import 'package:eclipse_test/network/models/dto_models/response/posts.dart';
import 'package:eclipse_test/network/repository/global_repository.dart';
import 'package:eclipse_test/screens/detailed_user_screen/bloc/bloc_detailed_user_screen.dart';
import 'package:eclipse_test/screens/users_preview_screen/bloc/bloc_users_preview.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

part 'events.dart';

part 'states.dart';

part 'parts/read.dart';

part 'parts/search.dart';

class BlocPostsScreen extends Bloc<EventBlocPosts, StateBlocPosts> {
  BlocPostsScreen({
    required this.repository,
    required this.blocUsersPreview,
  }) : super(StatePostsLoading()) {
    on<EventInitialPostsScreen>(_read);
    on<EventsSearchPostByUser>(_search);
  }

  final GlobalRepository repository;
  final BlocUsersPreview blocUsersPreview;
  List<PostsResponse>? posts;
  TextEditingController controller = TextEditingController();
}
