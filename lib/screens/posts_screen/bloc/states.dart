part of 'bloc_posts_screen.dart';

@immutable
abstract class StateBlocPosts {}

class StatePostsLoading extends StateBlocPosts {}

class StateLoadPosts extends StateBlocPosts {
  final List<PostsResponse> posts;
  final TextEditingController controller;

  StateLoadPosts({
    required this.posts,
    required this.controller,
  });
}

class StatePostsScreenError extends StateBlocPosts {
  final AppError error;

  StatePostsScreenError({
    required this.error,
  });
}
