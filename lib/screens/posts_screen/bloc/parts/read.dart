part of '../bloc_posts_screen.dart';

extension Read on BlocPostsScreen {
  Future<void> _read(
    EventInitialPostsScreen event,
    Emitter<StateBlocPosts> emit,
  ) async {
    try {
      List<String> urls = [
        "https://edit.co.uk/uploads/2016/12/Image-1-Alternatives-to-stock-photography-Thinkstock.jpg",
        "https://www.thephoblographer.com/wp-content/uploads/2020/04/WTFStockPhotos26.jpg?width=1200&enable=upscale",
        "https://video-images.vice.com/articles/5dfcdf5174a820009a1b4a41/lede/1576853802436-chicky-nuggies.jpeg?crop=1xw%3A1xh%3Bcenter%2Ccenter&resize=2000%3A*",
        "https://i.redd.it/uv33n76c5i851.jpg",
        "https://images.firstpost.com/wp-content/uploads/2020/04/meme02-3805.jpg?impolicy=website&width=1200&height=800",
        "https://www.techsmith.com/blog/wp-content/uploads/2021/09/Make-a-meme-butterfly.png",
        "https://64.media.tumblr.com/a7a92a23d7c9e0750c430ae56308d149/4946a2b1104ae2f0-28/s1280x1920/5615441219f2aa19a283ebcdefe9bc77711fdeca.png",
        "https://ic.pics.livejournal.com/jenya444/14462105/179284/179284_original.jpg",
        "https://www.michaelweeksagency.com/wp-content/uploads/2021/07/elan-must-meme.jpg",
        "https://cdn.vox-cdn.com/thumbor/nr3KfqoS4dGCLfEo6CEwovrnays=/1400x1050/filters:format(png)/cdn.vox-cdn.com/uploads/chorus_asset/file/19933026/image.png",
        "http://d279m997dpfwgl.cloudfront.net/wp/2021/03/what-could-go-wrong-1000x886.jpg",
      ];
      await Future.delayed(const Duration(seconds: 2)).then((value) async {
        if (event.userId == null) {
          posts = await repository.getPosts();
        } else {
          posts = await repository.getUserPosts(event.userId!);
        }
      });
      if(posts != null) {
        if (blocUsersPreview.users.isNotEmpty) {
          for (int i = 0; i < posts!.length; i++) {
            posts![i].user = blocUsersPreview.users
                .firstWhere((element) => element.id == posts![i].userId);
          }
        }
        Random gen = Random();
        for (int i = 0; i < posts!.length; i++) {
          DateTime randomDate = DateTime.now().subtract(
              Duration(days: gen.nextInt(62), minutes: gen.nextInt(240)));
          posts![i].date = randomDate;
          posts![i].imageUrl = urls[gen.nextInt(urls.length - 1)];
        }
        posts!.sort((a, b) => b.date!.compareTo(a.date!));
        emit(StateLoadPosts(posts: posts!, controller: controller,));
      }
    } catch (e) {
      emit(
        StatePostsScreenError(
          error: AppError(
            message: e.dioErrorMessage,
            code: e.dioErrorStatusCode,
          ),
        ),
      );
    }
  }
}
