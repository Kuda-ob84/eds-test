part of "../bloc_posts_screen.dart";

extension Search on BlocPostsScreen {
  Future<void> _search(
    EventsSearchPostByUser event,
    Emitter<StateBlocPosts> emit,
  ) async {
    try {
      if(event.isFromAnotherPage){
        controller.text = event.name;
      }
      emit(StateLoadPosts(
        posts: posts!
            .where((element) => element.user!.name!.contains(event.name))
            .toList(),
        controller: controller,
      ));
    } catch (e) {
      emit(
        StatePostsScreenError(
          error: AppError(
            message: e.dioErrorMessage,
            code: e.dioErrorStatusCode,
          ),
        ),
      );
    }
  }
}
