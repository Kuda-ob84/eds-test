import 'package:eclipse_test/screens/bottom_navigation_bar/bottom_navigation_bar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:hive/hive.dart';
import 'package:provider/src/provider.dart';
import 'package:path_provider/path_provider.dart';

import 'generated/l10n.dart';
import 'main/dependency_initializer/dependency_initializer.dart';
import 'main/dependency_provider/dependency_provider.dart';
import 'main/top_level_blocs/top_level_blocs.dart';
import 'network/dio_wrapper/dio_wrapper.dart';
import 'network/repository/global_repository.dart';
import 'network/repository/hive_repository.dart';
import 'network/services/network_service.dart';

const String baseUrl = 'https://jsonplaceholder.typicode.com/';

String get projectBaseUrl {
  if (kDebugMode) return baseUrl;
  return '';
}

void main() async {
  ///Global managers initialization
  Future<bool> _initialize(BuildContext context) async {
    try {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);

      final docDir = await getApplicationDocumentsDirectory();
      Hive.init(docDir.path);
      await context.read<HiveRepository>().init();
      await context.read<DioWrapper>().init(
            baseURL: projectBaseUrl,
            globalRepository: context.read<GlobalRepository>(),
          );
      context.read<NetworkService>().init(context.read<DioWrapper>(), context.read<HiveRepository>());
      context
          .read<GlobalRepository>()
          .init(context.read<NetworkService>(), context.read<HiveRepository>());
    } catch (e, stackTrace) {

    }
    return true;
  }

  runApp(
    DependenciesProvider(
      child: TopLevelBlocs(
        child: MaterialApp(
          title: 'EDS Test',
          debugShowCheckedModeBanner: false,
          localizationsDelegates: const [
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: S.delegate.supportedLocales,
          theme: ThemeData(
            appBarTheme: const AppBarTheme(
              backgroundColor: Colors.white,
            ),
            scaffoldBackgroundColor: Colors.white,
            fontFamily: 'Gilroy',
            textSelectionTheme: const TextSelectionThemeData().copyWith(
              cursorColor: Colors.black,
            ),
          ),
          home: DependenciesInitializer(
            loadingIndicatorScreen: const Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            ),
            initializer: _initialize,
            child: BottomNavigationBarScreen(),
          ),
        ),
      ),
    ),
  );
}

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBarScreen();
  }
}
