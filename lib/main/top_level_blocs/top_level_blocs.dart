import 'package:eclipse_test/network/repository/global_repository.dart';
import 'package:eclipse_test/screens/bottom_navigation_bar/cubit/bottom_nav_bar_cubit.dart';
import 'package:eclipse_test/screens/posts_screen/bloc/bloc_posts_screen.dart';
import 'package:eclipse_test/screens/users_preview_screen/bloc/bloc_users_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

///Providers for global blocs
class TopLevelBlocs extends StatelessWidget {
  final Widget child;

  const TopLevelBlocs({required this.child});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<BottomNavBarCubit>(
          create: (context) => BottomNavBarCubit(),
        ),
        BlocProvider<BlocUsersPreview>(
          create: (context) => BlocUsersPreview(
            repository: context.read<GlobalRepository>(),
          )..add(
              EventInitialUsersPreview(),
            ),
        ),
        BlocProvider<BlocPostsScreen>(
          create: (context) => BlocPostsScreen(
            repository: context.read<GlobalRepository>(),
            blocUsersPreview: context.read<BlocUsersPreview>(),
          )..add(
              EventInitialPostsScreen(),
            ),
        ),
      ],
      child: child,
    );
  }
}
