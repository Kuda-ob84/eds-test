import 'package:eclipse_test/network/dio_wrapper/dio_wrapper.dart';
import 'package:eclipse_test/network/repository/global_repository.dart';
import 'package:eclipse_test/network/repository/hive_repository.dart';
import 'package:eclipse_test/network/services/network_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

///Providers for global managers
class DependenciesProvider extends StatelessWidget {
  final Widget child;

  const DependenciesProvider({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        // RepositoryProvider(
        //   create: (_) => SyncSharedPreferences(),
        // ),
        RepositoryProvider(
          create: (_) => DioWrapper(),
        ),
        RepositoryProvider(
          create: (_) => GlobalRepository(),
        ),
        RepositoryProvider(
          create: (_) => NetworkService(),
        ),
        RepositoryProvider(
          create: (_) => HiveRepository(),
        ),
      ],
      child: child,
    );
  }
}
